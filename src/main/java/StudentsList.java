import java.util.*;

public class StudentsList {

    public static void main(String[] args) {
        String[] studentsArray = {"Антонов", "Шевченко", "Бойко", "Шевченко", "Кравченко", "Савицький", "Антонов", "Шевченко", "Тетерів", "Мамченко"};
        List<String> students = new ArrayList<>();

        for (String student : studentsArray) {
           students.add(student);
        }
        System.out.println("Студентів у списку: " + students.size());

        Set<String> uniqueStudents = new HashSet<>();
        for (String uniqueStudent : studentsArray) {
            uniqueStudents.add(uniqueStudent);
        }
        System.out.println("Унікальні студенти: " + uniqueStudents.size());

        Map<String, Integer> studentsLetters = new HashMap<>();
        studentsLetters.put("Антонов", 7);
        studentsLetters.put("Шевченко", 8);
        studentsLetters.put("Бойко", 5);
        studentsLetters.put("Шевченко", 8);
        studentsLetters.put("Кравченко", 9);
        studentsLetters.put("Савицький", 9);
        studentsLetters.put("Антонов", 7);
        studentsLetters.put("Шевченко", 8);
        studentsLetters.put("Тетерів", 7);
        studentsLetters.put("Мамченко", 8);

        for (Map.Entry<String, Integer> entry : studentsLetters.entrySet()) {
            System.out.println("Прізвище: " + entry.getKey() + " Кількість літер: " + entry.getValue());
        }
    }
}
